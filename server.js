const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt-nodejs');
const cors = require('cors');
const app = express();
const Nexmo = require('nexmo')
const nexmo = new Nexmo({
    apiKey: '4ee576d0',
    apiSecret: 'yNOH7r30nUtqmUOv'
});
const knex = require('knex')

const db = knex({
    client: 'pg',
    connection: {
        host : '127.0.0.1',
        user : 'postgres',
        password : 'admin',
        database : 'restaurant_web'
    }
});

app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));

const number = 918830374665

app.get('/', (req, res) => {
    res.json("Private data");
})

app.post('/signin', (req, res) => {
    db.select('email','password').from('user_details')
        .where('email','=',req.body.email)
        .then(data => {
            const isValid = bcrypt.compareSync(req.body.password , data[0].password)
            if(isValid){
                db.select('*').from('user_details')
                    .where('email','=',req.body.email)
                    .then(user => {
                        res.json(user[0])
                    })
                    .catch(err => res.status(400).json('unable to get user'))
            }
        })
        .catch(err => res.status(400).json('Invalid credential'))
})

app.post('/signup', (req, res) => {
    const {name, email, password} = req.body;
    const hash = bcrypt.hashSync(password)
    db('user_details')
        .returning('*')
        .insert({
        name : name,
        email: email,
        password: hash,
        date : new Date()
    })
        .then(user => {
            res.json(user[0])
        })
        .catch(error => {
            res.status(400).json(error)
        })
})

app.post('/profile/user', (req, res) => {
    const {email} = req.body;
    db.select('id','name','email').from('user_details')
        .where('email','=',email)
        .then(data => {
            res.json(data)
        })
        .catch(error => res.status(400).json("not found"))
})

app.post('/profile/contact',(req,res) => {
    const {id} = req.body;
    db.select('mobile').from('contacts')
        .where('user_id','=',id)
        .then(data => {
                res.json(data)
        })
        .catch(error => res.status(400).json('not found'))
})

app.post('/profile/address', (req,res) => {
    const {id} = req.body;
    db.select('*').from('addresses')
        .where('user_id','=',id)
        .then(data => {
            res.json(data);
        })
        .catch(error => res.status(400).json("not found"))
})

app.get('/menu', (req, res) => {
    db.select('*').from('menus')
        .then(data => {
            res.json(data)
        })
        .catch(error => res.status(400).json('Unable to fetch menus'))
})

app.post('/send', (req, res) => {
    // Send SMS
    const {toNumber, message} = req.body
    nexmo.message.sendSms(
        number, toNumber, message, {type: 'unicode'},
        (err, responseData) => {
            if (responseData) {
                console.log(responseData)
            }
        }
    )
    res.json('success')
});

app.get('/menu/food_category', (req, res) => {
    db.select('menu_type').from('food_category')
        .then(data => {
            res.json(data)
        })
        .catch(error => res.status(400).json('error in getting categories'))
})

app.post('/menu/order', (req, res) => {
    const {user_id,order,mob_no,type_of_add,user_add,landmark,total} = req.body;
    db('orders')
        .returning('*')
        .insert({
            user_id:user_id,
            orders : JSON.stringify(order),
            mob_no : mob_no,
            type_of_add:type_of_add,
            user_add:user_add,
            landmark:landmark,
            total:total,
            date_of_order:new Date()
        })
        .then(order => {
            res.json(order[0])
        })
        .catch(error=>{
            res.status(400).json(error)
        })
})

app.post('/profile/contact/add' , (req,res) =>{
    const {user_id,mobile} = req.body;
    db('contacts')
        .returning('*')
        .insert({
            user_id : user_id,
            mobile : mobile
        })
        .then(data => {
            res.json(data)
        })
        .catch(error => res.status(400).json('not entered'))
})

app.post('/profile/address/add',(req,res) => {
  const {user_id,type,landmark,complete_address} = req.body;
    db('addresses')
        .returning('*')
        .insert({
            user_id:user_id,
            type : type,
            landmark : landmark,
            complete_address:complete_address
        })
        .then(data => {
            res.json(data)
        })
        .catch(error =>res.status(400).json('not entered'))
})

app.listen(3000);
console.log("server listning on port 3000");